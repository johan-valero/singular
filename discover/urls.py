from django.urls import path 
from . import views

app_name = 'discover'
urlpatterns = [
    path('', views.index, name= 'index'),
    path('<int:publication_id>/detail/', views.detail, name='detail'),
    path('resultat/', views.resultat, name='resultat'),
    path('category/', views.category, name='category'),
    path('<int:category_id>/list_category/', views.list_category, name='list_category'),
    path('register/', views.register, name='register'),
    path('registered/', views.registered, name='registered'),
    path('login/', views.my_login, name='login'),
    path('logout/', views.my_logout, name='logout'),
    path('welcome/', views.welcome, name='welcome'),
    path('published/', views.published, name='published'),
]
