from django.shortcuts import render, get_object_or_404
from django.http import HttpResponse
from django.contrib.auth.models import User
from django.contrib.auth import authenticate, login , logout
from . models import Publication, Users, Adress, Statut, Creator, Layout, TypeLayout, SubCategory, Category, Favorite, Rate

# Create your views here.

# PLACES 
def index(request):
    last_publish = Publication.objects.order_by('id')
    category =  Category.objects.all()
    subcategory =  SubCategory.objects.all()
    context = {
        'publish_list': last_publish,
        'category': category ,
        'subcategory': subcategory
    }
    return render(request, 'discover/index.html', context)

def detail(request, publication_id):
    detail = get_object_or_404(Publication, pk=publication_id)
    context = {'detail': detail}
    return render(request, 'discover/detail.html', context)

def resultat(request):
    context = {}
    publication_name = []

    if request.method == "POST":
        search_input = request.POST['search_input'].lower()
        words = search_input.split(" ")
        
        #Search by name publication 
        for word in words:
            publication = Publication.objects.filter(name_publication__contains=word)
            for pub in publication:
                publication_name.append(pub)
                
        #Search by date publication
        # publication_date = Publication.objects.filter(date_publication__contains=word)

        #Search by adress publication 
        # publication_adress = Publication.objects.filter(adress__contains=search_input)

        #Search by creator
        # publication_creator = Publication.objects.filter(creator__contains=search_input)

        #Search by layout 
        # publication_layout = Publication.objects.filter(layout__name_layout__contains=search_input)

        # Search by category
        # publication_category = Publication.objects.filter(category__name_category__contains=search_input)

        context = {
            'search_input': search_input,
            'publication' :publication,
            # 'publication_date': publication_date,
            # 'publication_creator': publication_creator,
            # 'publication_adress': publication_adress,
            # 'publication_creator': publication_creator,
            # 'publication_layout': publication_layout,
            # 'publication_category': publication_category,
        }

        return render(request, 'discover/resultat.html', context)
    else:
        return render(request, 'discover/resultat.html', context)


# CATEGORY

def category(request):
    category = Category.objects.order_by('id')
    subcategory = SubCategory.objects.order_by('id')
    context = {
        'category': category,
        'subcategory': subcategory,
    }
    return render(request, 'discover/category/category.html', context)   

# def list_category(request, category_id):
#     category1 = get_object_or_404(Category, pk=category_id)
#     list_publication = Publication.objects.filter(category_id=category_id).values()
#     context = {
#         'category':category1,
#         'list_publication': list_publication,
#     }
#     return render(request, 'discover/category/list_category.html', context)

def list_category(request, category_id):
    categ = get_object_or_404(Category, pk=category_id)
    liste_publication = Publication.objects.filter(category=category_id).values('id', 'name_publication', 'date_publication', 'creator', 'category', 'pictures_publication')
    for pub in liste_publication:
        ca = get_object_or_404(Category, pk=pub['category'])
        sc = get_object_or_404(SubCategory, pk=pub['subcategory'])
        c1 = get_object_or_404(Creator, pk=pub['creator'])

        pub['category']={
            'id':ca.id, 
            'name_category':ca.name_category, 
            'avatar_category':ca.avatar_category
            }

        pub['subcategory']={
            'id':sc.id, 
            'name_subCategory':sc.name_subCategory
            }

        pub['creator']={
            'id':c1.id, 
            'name_creator':c1.name_creator, 
            'firstname_creator':c1.firstname_creator, 
            'birthdate_creator':c1.birthdate_creator, 
            'avatar_creator':c1.avatar_creator
            }

    context = {
        'categ':categ, 
        'liste_publication':liste_publication}
    return render(request, 'discover/category/list_category.html', context)

# ACCOUNT

def my_login(request):
    return render(request, 'discover/account/login.html')

def welcome(request):
    username = request.POST['username']
    password = request.POST['password']
    user = authenticate(request, username=username, password=password)
    context = {'user':user}
    if user is not None :
        login(request, user)
        return render(request, 'discover/account/welcome.html', context)
    else:
        return render(request, 'discover/account/error_log.html')

def my_logout(request):
    logout(request)
    return render(request, 'discover/account/logout.html')

def register(request):
    return render(request, 'discover/account/register.html')

def registered(request):
    name = request.POST['user_name']
    firstname = request.POST['user_firstname']
    pwd = request.POST['user_pwd']
    email = request.POST['user_email']
    avatar = request.POST['user_avatar']
    username = firstname[0].lower() + "." + name.lower()
    user = User.objects.create_user(username, email, pwd)
    utilisateur = Users(user=user)
    user.last_name = name
    user.first_name = firstname
    utilisateur.avatar_user = avatar
    user.save()
    utilisateur.save()
    context = {'user':user}
    return render(request, 'discover/account/registered.html', context)
        # return render(request, 'discover/error_log_inscription.html')

# PUBLICATION 

def published(request):
    # lieu = request.POST['lieu']
    # creator = request.POST['creator']
    # category = request.POST['category']
    # avatar_publication = request.POST['avatar_publication']
    # publication = Publication.objects.create_publication(name_publication, )
    return render(request, 'discover/account/published.html')